/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanisata.finaleoop;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class BookNDatetoBorrow implements Serializable{
     private String bookname;
     private int date;

    public BookNDatetoBorrow(String bookname, int date) {
        this.bookname = bookname;
        this.date = date;
    }

    public String getBookname() {
        return bookname;
    }

    public void setBookname(String bookname) {
        this.bookname = bookname;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Book {" + "Bookname :" + bookname + " ,  Date :" + date + " week  }";
    }
     
}
