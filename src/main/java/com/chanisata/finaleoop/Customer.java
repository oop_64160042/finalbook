/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanisata.finaleoop;

import java.io.Serializable;

/**
 *
 * @author ASUS
 */
public class Customer implements Serializable{
    private String name;
    private String tel;
    private String email;
    
    public Customer(String name, String tel, String email) {
        this.name = name;
        this.tel =  tel;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    @Override
    public String toString() {
        return "Customer {" + "Name :" + name + ", Tel. :" + tel + ", E-mail :" + email + "}";
                }
}
